﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
namespace MCMan
{
    public partial class Console : Form
    {
        Process p;
        public Console(Process P)
        {
            p = P;
            InitializeComponent();
            p.OutputDataReceived += new DataReceivedEventHandler(InstOutput);
            p.ErrorDataReceived += new DataReceivedEventHandler(InstOutput);
            //Create the event for when the process is terminated
            p.Exited += (o, args) => Message(string.Format("Minecraft exited with code: {0}" , (o as Process).ExitCode.ToString()));
            p.Start();
            p.BeginOutputReadLine();
            p.BeginErrorReadLine();
        }
        public void Message(string text)
        {
            if (text == null) return;
            //Using lambda to handle asynchronus issues
            Invoke((Action)(() => textBox1.AppendText(text +'\n')));
        }

        void InstOutput(object sender, DataReceivedEventArgs e)
        {
            Message(e.Data);
        }

        private void closeBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
