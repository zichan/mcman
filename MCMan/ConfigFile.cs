﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace MCMan
{
    class ConfigFile
    {
        protected Dictionary<string, string> dict;
        public ConfigFile()
        {
            dict = new Dictionary<string, string>();
        }
        public virtual void Save(string sPath)
        {
            using (Stream stream = File.Open(sPath, FileMode.Create))
            {
                Save(stream);
            }
        }
        public virtual void Save(Stream stream)
        {
            string[] lines = new string[dict.Count];
            int i = 0;
            foreach (KeyValuePair<string, string> kv in dict)
            {
                lines[i++] = kv.Key + "=" + kv.Value;
            }
            using (StreamWriter sw = new StreamWriter(stream))
            {
                foreach (string s in lines)
                {
                    sw.WriteLine(s);
                }
            }
        }
        public virtual void Load(string path)
        {
            try
            {
                using (Stream s = File.OpenRead(path))
                {
                    Load(s);
                }
            }
            catch (FileNotFoundException)
            {
            }
        }
        public virtual void Load(Stream stream)
        {
            using (StreamReader sr = new StreamReader(stream))
            {
                dict.Clear();
                string[] lines = sr.ReadToEnd().Split('\n').Select(l => l.TrimEnd('\n', '\r')).ToArray();
                foreach (string line in lines)
                {
                    if (!line.StartsWith("#"))
                    {
                        string[] linedata = line.Split('=');
                        if (linedata.Length >= 2)
                        {
                            dict[linedata[0]] = linedata[1];
                        }
                    }
                }
            }
        }
        public string this[string key]
        {
            get
            {
                return dict[key];
            }
            set
            {
                dict[key] = value;
            }
        }
        public bool Remove(string key)
        {
            return dict.Remove(key);
        }
    }
}
