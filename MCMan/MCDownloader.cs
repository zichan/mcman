﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using System.Diagnostics;
namespace MCMan
{
    class MCDownloader
    {
        System.Windows.Forms.ProgressBar pb;
        System.Windows.Forms.Form fm;
        string mainSite = "http://s3.amazonaws.com/MinecraftDownload/";
        string lwjglSite = "http://www.newdawnsoftware.com/jenkins/job/LWJGL/lastStableBuild/artifact/dist/";
        Queue<string> downloadUrl = new Queue<string>();
        Queue<string> downloadFile = new Queue<string>();
        string binPath;
        Thread th;
        public MCDownloader(string path)
        {
            binPath = path;
            downloadUrl.Enqueue(mainSite + "minecraft.jar");
            downloadUrl.Enqueue(lwjglSite + "lwjgl-2.8.4.zip");
            downloadFile.Enqueue(path + "\\.minecraft\\bin\\minecraft.jar");
            downloadFile.Enqueue(path + "\\.minecraft\\bin\\lwjgl.zip");
        }
        public void downloadFiles(WebClient wc)
        {
            if (downloadFile.Count != 0)
            {
                string url = downloadUrl.Dequeue();
                wc.DownloadFileAsync(new Uri(url), downloadFile.Dequeue());
                wc.DownloadFileCompleted += (o, args) => downloadFiles(wc);
            }
            else if (th == null)
            {

                fm = new System.Windows.Forms.Form();
                fm.Size = new System.Drawing.Size(200, 100);
                System.Windows.Forms.Label lab = new System.Windows.Forms.Label();
                lab.Location = new System.Drawing.Point(3, 3);
                lab.Text = "Extracting files";
                pb = new System.Windows.Forms.ProgressBar();
                pb.Size = new System.Drawing.Size(180, pb.Size.Height);
                pb.Location = new System.Drawing.Point(3, lab.Location.X + lab.Size.Height + 5);
                fm.Controls.Add(lab);
                fm.Controls.Add(pb);
                fm.Show();
                th = new Thread(extractFiles);
                th.Start();
            }
        }
        private void extractFiles()
        {
            ZipInputStream zs = new ZipInputStream(File.OpenRead(binPath + "\\.minecraft\\bin\\lwjgl.zip"));
            while (true)
            {
                ZipEntry zipEntry = zs.GetNextEntry();
                if (zipEntry == null)
                    break;
                String entryFileName = Path.GetFileName(zipEntry.Name);
                if (entryFileName == "jinput.jar" || entryFileName == "lwjgl_util.jar" || entryFileName == "lwjgl.jar" || entryFileName.EndsWith(".dll"))
                {
                    // can test filenames and exclude some
                    byte[] buffer = new byte[4096];
                    String fullZipToPath;
                    if(entryFileName.EndsWith(".dll"))
                    {
                        fullZipToPath = Path.Combine(@binPath + "\\.minecraft\\bin\\natives\\", entryFileName);
                    }
                    else
                    {
                        fullZipToPath = Path.Combine(@binPath + "\\.minecraft\\bin\\", entryFileName);
                    }
                    // unzip file
                    fm.Invoke((Action)(() => pb.Maximum = (int)zs.Length));
                    //pb.Maximum = (int)zs.Length;
                    using (FileStream streamWriter = File.Create(fullZipToPath))
                    {
                        while (true)
                        {
                            int nBytes = zs.Read(buffer, 0, buffer.Length);
                            if (nBytes <= 0)
                                break;
                            streamWriter.Write(buffer, 0, nBytes);
                            if (!(pb.Maximum < (pb.Value + 4096)))
                            {
                                pb.Invoke((Action)(() => pb.Value += 4096));
                            }
                        }
                    }
                    pb.Invoke((Action)(() => pb.Value = 0));
                }
            }
            fm.Invoke((Action)(() =>
                {
                    fm.Close();
                    zs.IsStreamOwner = true;
                    zs.Close();
                }));
            try
            {
                File.Copy(binPath + "\\.minecraft\\bin\\minecraft.jar", binPath + "\\.minecraft\\bin\\minecraftbackup.jar");
            }
            catch
            {
            }
        }
    }
}
