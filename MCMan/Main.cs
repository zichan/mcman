﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Web;
using System.Net;

namespace MCMan
{
    public partial class Main : Form
    {
        List<Instance> instList;
        public Main()
        {

            InitializeComponent();
            instList = new List<Instance>();
            scanForInstances();
        }
        private void scanForInstances()
        {
            string exePath = Path.GetDirectoryName(Application.ExecutablePath);
            string instPath = exePath + "\\instances";
            if (!Directory.Exists(instPath))
            {
                Directory.CreateDirectory(instPath);
            }
            int Width = 10;
            foreach (string dir in Directory.GetDirectories(instPath))
            {
                if (File.Exists(dir + "/inst.cfg"))
                {
                    Instance inste = new Instance(dir);
                    instList.Add(inste);
                    addInstButton(inste, Width);
                    Width += 70;
                }
            }
        }
        private void addInstButton(Instance inst, int Width)
        {
            if (instList.Count <= 1)
            {
                inst.Left = 10;
                inst.Top = toolStrip1.Size.Height;
            }
            else
            {
                inst.Location = new Point(Width, 25);
            }
            inst.TabStop = false;
            inst.Enabled = true;
            inst.Size = new System.Drawing.Size(70,60);
            inst.Visible = true;
            flowLayoutPanel1.Controls.Add(inst);
        }
        WebClient wc;
        int numb;
        private void createInst()
        {
            numb = 0;
            InputBox inp = new InputBox();
            string responce = "";
            inp.okBtn.Click += (o, args) => responce = inp.textBox1.Text;
            inp.ShowDialog(); 
            char[] illegal = new char[] { '!', '@', '#', '$', '%', '_', '^' };

            foreach (char c in illegal)
            {
                if (responce.Contains(c))
                {
                    MessageBox.Show("Name cannont contain !,@,#,%,_, or ^");
                    responce = null;
                    continue;
                }
            }
            if (responce != null)
            {
                Instance tempinst = new Instance(Path.GetDirectoryName(Application.ExecutablePath) + "/instances", responce);
                instList.Add(tempinst);
                statusStrip1.Visible = true;
                wc = new WebClient();
                wc.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
                wc.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);
                tempinst.populateInst(wc);
                numb = 1;
            }
        }
        private void newInstanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string item = (sender as ToolStripItem).Text;
            if (item == "New Instance")
            {
                createInst();
            }
        }
        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            toolStripProgressBar1.Value = e.ProgressPercentage;
        }
        private void Completed(object sender, AsyncCompletedEventArgs e)
        {
            toolStripProgressBar1.Value = 0;
            if (numb == 1)
            {
                numb++;
            }
            else if (numb == 2 && !wc.IsBusy)
            {
                statusStrip1.Visible = false;
                instList.Clear();
                while (flowLayoutPanel1.Controls.Count > 0)
                    flowLayoutPanel1.Controls.RemoveAt(0);
                scanForInstances();
            }
        }
    }
}
