﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using ICSharpCode.SharpZipLib.Zip;
namespace MCMan
{
    public sealed class Instance : System.Windows.Forms.Button
    {
        Process instProc;
        ConfigFile cf;

#region Folder paths
        String dotMC
        {
            get
            {
                return Path.Combine(mainPath + "\\.minecraft");
            }
        }
        String MCMods
        {
            get
            {
                return Path.Combine(dotMC + "\\mods");
            }
        }
        String texturePack
        {
            get
            {
                return Path.Combine(dotMC + "\\texturepack");
            }
        }
        String binDir
        {
            get
            {
                return Path.Combine(dotMC + "\\bin");
            }
        }
        String nativesDir
        {
            get
            {
                return Path.Combine(binDir + "\\natives");
            }
        }
        String jarMods
        {
            get
            {
                return Path.Combine(dotMC + "\\jarMods");
            }
        }
#endregion
        public string mainPath;
        string Xmx = string.Format("-Xmx{0}M", 1024);
        public Instance(string path)
        {
            mainPath = path;
            cf = new ConfigFile();
            cf.Load(path + "/inst.cfg");
            this.Text = cf["name"];
            FlatAppearance.BorderSize = 0; FlatAppearance.BorderColor = Color.FromArgb(51, 153, 255);
            FlatAppearance.CheckedBackColor = Color.FromArgb(153, 204, 255);
            FlatAppearance.MouseDownBackColor = Color.FromArgb(153, 204, 255);
            FlatAppearance.MouseOverBackColor = Color.FromArgb(194, 224, 255);
            FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            #region Create the menu
            ContextMenu men = new ContextMenu();
            men.MenuItems.Add(new MenuItem("Play", (o, args) =>
                {
                    Launch();
                }));
            men.MenuItems.Add(new MenuItem("-"));
            men.MenuItems.Add(new MenuItem("Manage Mods",(o,args) => 
                {
                    ModManager mm = new ModManager(dotMC);
                    mm.ShowDialog();
                    if (mm.jarModChange)
                    {
                        rebuildJar();
                    }
                }));
            men.MenuItems.Add(new MenuItem("Rebuild Jar", (o, args) =>
                {
                    rebuildJar();
                }));

            men.MenuItems.Add(new MenuItem("-"));
            men.MenuItems.Add(new MenuItem("Delete", (o, args) =>
                {
                    deleteInst();
                }));
            this.ContextMenu = men;
            #endregion
            //this.Image = global::MCMan.Properties.Resources.Iconbtn;
         }
        public Instance(string instancesPath, string instName)
        {
            mainPath = instancesPath + "/" + instName;
            this.Create(instancesPath, instName);
        }
        public void Create(string path, string name)
        {
            Directory.CreateDirectory(mainPath);
            try
            {
                File.Create(mainPath + "/inst.cfg").Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            Directory.CreateDirectory(dotMC);
            Directory.CreateDirectory(binDir);
            Directory.CreateDirectory(nativesDir);
            Directory.CreateDirectory(texturePack);
            Directory.CreateDirectory(jarMods);
            Directory.CreateDirectory(MCMods);
            cf = new ConfigFile();
            cf["name"] = name;
            cf.Save(mainPath + "/inst.cfg");
            //populateInst(wc);
        }
        public void Launch()
        {
            //Create the arguments to pass to java.exe
            //Using escape character \ to avoid spaces in paths veing mistaken for agruments
            String executionArgs = string.Format(
                "{0} -Djava.library.path=\"{1}\" -cp \"{2}\"\\minecraft.jar;\"{2}\"\\jinput.jar;\"{2}\"\\lwjgl.jar;\"{2}\"\\lwjgl_util.jar net.minecraft.client.Minecraft", Xmx, nativesDir, binDir); 
            instProc = new Process();
            instProc.StartInfo.UseShellExecute = false;
            //Redirects the APPDATA environment variable to main directory for modloader and worlds to load corectlly for seperate instances
            instProc.StartInfo.EnvironmentVariables["APPDATA"] = mainPath;
            // Redirect the output stream of the child process.
            instProc.StartInfo.RedirectStandardOutput = true;
            instProc.StartInfo.RedirectStandardError = true;
            //Prevents the cmd window from showing
            instProc.StartInfo.CreateNoWindow = true;
            instProc.EnableRaisingEvents = true;
            instProc.StartInfo.FileName = @"C:\Program Files\Java\jre6\bin\java.exe";
            instProc.StartInfo.Arguments = executionArgs;
            //Create the Console window with the process
            Console c = new Console(instProc);
            c.Show();
        }
        public void populateInst(WebClient wc)
        {
            MCDownloader mcd = new MCDownloader(mainPath);
            mcd.downloadFiles(wc);
        }
        public void rebuildJar()
        {
            File.Copy(mainPath + "\\.minecraft\\bin\\minecraft.jar", mainPath + "\\.minecraft\\bin\\minecraftbackup.jar");
            ZipFile zf = new ZipFile(mainPath + "\\.minecraft\\bin\\minecraft.jar");
            zf.Delete("META-INF");
            //add mod stuff here
            zf.Close();
        }
        public void deleteInst()
        {

        }
        private bool ShowBorder { get; set; }
        #region Override events
        protected override void OnClick(EventArgs args)
        {
            this.Launch();
        }
        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);
            ShowBorder = true;
        }
        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);

            ShowBorder = false;
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            // The DesignMode check here causes the border to always draw in the Designer
            // This makes it easier to place your button
            if (DesignMode || ShowBorder)
            {
                Pen pen = new Pen(FlatAppearance.BorderColor, 1);
                Rectangle rectangle = new Rectangle(0, 0, Size.Width - 1, Size.Height - 1);
                e.Graphics.DrawRectangle(pen, rectangle);
            }
        }
        #endregion
    }
}
